package com.example.dinozoo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapter(context : MainActivity, bands:List<String>):
                    RecyclerView.Adapter<MyAdapter.ViewHolder>(){
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text=bands.get(position)
    }

    override fun getItemCount(): Int {
        return bands.count()
    }

    var bands :List<String> = bands
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter.ViewHolder {
        val  layoutInflater=LayoutInflater.from(parent.context)
        val cellForRow:TextView=layoutInflater.inflate(R.layout.row,parent,false)
        as TextView
        return ViewHolder(cellForRow)

    }





    class ViewHolder( val textView: TextView):RecyclerView.ViewHolder(textView)
}

