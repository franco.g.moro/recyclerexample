package com.example.dinozoo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var recyclerView:RecyclerView=findViewById(R.id.recycler)
        var bands: List<String> = listOf("Led Zeppelin","The clash","The animals","SmashMouth","Sound Garden")
        recyclerView.layoutManager= LinearLayoutManager(this)
        recyclerView.adapter =MyAdapter(this,bands)





    }
}
